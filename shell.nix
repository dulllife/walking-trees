{ nixpkgs ? import <nixpkgs> {} }:

# From https://maybevoid.com/posts/2019-01-27-getting-started-haskell-nix.html

let
  inherit (nixpkgs) pkgs;
  inherit (pkgs) haskellPackages;

  haskellDeps = ps: with ps; [
    base
    # Look at separating test deps if that even makes sense
    tasty
  ];

  ghc = haskellPackages.ghcWithPackages haskellDeps;

  nixPackages = [
    ghc
    pkgs.gdb
    haskellPackages.cabal-install
  ];
in
pkgs.stdenv.mkDerivation {
  name = "env";
  buildInputs = nixPackages;
}
