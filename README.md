# Walking Trees

An experimental data structure for geospatial indexing. Find the cabal file for a little more information. Eventually, look at hackage.

### Dev setup

With [nix](https://nixos.org/nix/) installed.

```
$ nix-build release.nix # Build it
$ result/bin/walking-trees # Run it
```

