{ mkDerivation, base, stdenv }:
mkDerivation {
  pname = "walking-trees";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base ];
  description = "An experimental geospatial data structure";
  license = "unknown";
  hydraPlatforms = stdenv.lib.platforms.none;
}
